#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet at
# http://www.illumos.org/license/CDDL.
#

[DEFAULT]
pre = setup
quiet = False
pre_user = root
user = root
timeout = 600
post_user = root
post = cleanup
outputdir = /var/tmp/test_results
tags = ['functional']

[tests/functional/acl/posix]
tests = ['posix_001_pos', 'posix_002_pos', 'posix_003_pos']
tags = ['functional', 'acl', 'posix']

[tests/functional/alloc_class]
tests = ['alloc_class_001_pos', 'alloc_class_002_neg']
tags = ['functional', 'alloc_class']

[tests/functional/arc]
tests = ['dbufstats_001_pos', 'dbufstats_002_pos']
tags = ['functional', 'arc']

[tests/functional/atime]
tests = ['atime_001_pos', 'atime_002_neg', 'atime_003_pos', 'root_atime_off',
    'root_atime_on', 'root_relatime_on']
tags = ['functional', 'atime']

[tests/functional/cache]
tests = ['cache_001_pos', 'cache_002_pos', 'cache_003_pos', 'cache_004_neg']
tags = ['functional', 'cache']

[tests/functional/cachefile]
tests = ['cachefile_001_pos', 'cachefile_002_pos', 'cachefile_003_pos',
    'cachefile_004_pos']
tags = ['functional', 'cachefile']

[tests/functional/chattr]
tests = ['chattr_001_pos', 'chattr_002_neg']
tags = ['functional', 'chattr']

[tests/functional/checksum]
tests = ['run_edonr_test', 'run_sha2_test', 'run_skein_test',
    'filetest_001_pos']
tags = ['functional', 'checksum']

[tests/functional/clean_mirror]
tests = [ 'clean_mirror_001_pos', 'clean_mirror_002_pos',
    'clean_mirror_003_pos', 'clean_mirror_004_pos']
tags = ['functional', 'clean_mirror']

[tests/functional/cli_root/zdb]
tests = ['zdb_001_neg', 'zdb_002_pos']
pre =
post =
tags = ['functional', 'cli_root', 'zdb']

[tests/functional/cli_root/zfs]
tests = ['zfs_001_neg', 'zfs_002_pos']
tags = ['functional', 'cli_root', 'zfs']

[tests/functional/cli_root/zfs_create]
tests = ['zfs_create_001_pos', 'zfs_create_encrypted', 'zfs_create_crypt_combos']
tags = ['functional', 'cli_root', 'zfs_create']

[tests/functional/cli_root/zfs_destroy]
tests = ['zfs_destroy_001_pos']
tags = ['functional', 'cli_root', 'zfs_destroy']

[tests/functional/cli_root/zfs_diff]
tests = ['zfs_diff_changes', 'zfs_diff_cliargs', 'zfs_diff_timestamp',
    'zfs_diff_types', 'zfs_diff_encrypted']
tags = ['functional', 'cli_root', 'zfs_diff']

[tests/functional/cli_root/zfs_get]
tests = ['zfs_get_001_pos']
tags = ['functional', 'cli_root', 'zfs_get']

[tests/functional/cli_root/zfs_mount]
tests = ['zfs_mount_001_pos', 'zfs_mount_encrypted']
tags = ['functional', 'cli_root', 'zfs_mount']

[tests/functional/cli_root/zfs_rename]
tests = ['zfs_rename_001_pos', 'zfs_rename_to_encrypted', 'zfs_rename_mountpoint']
tags = ['functional', 'cli_root', 'zfs_rename']

[tests/functional/cli_root/zfs_send]
tests = ['zfs_send_001_pos', 'zfs_send_encrypted', 'zfs_send_raw']
tags = ['functional', 'cli_root', 'zfs_send']

[tests/functional/cli_root/zfs_snapshot]
tests = ['zfs_snapshot_001_neg', 'zfs_snapshot_002_neg']
tags = ['functional', 'cli_root', 'zfs_snapshot']

[tests/functional/cli_root/zfs_sysfs]
tests = ['zfeature_set_unsupported.ksh', 'zfs_get_unsupported',
    'zfs_set_unsupported', 'zfs_sysfs_live.ksh', 'zpool_get_unsupported',
    'zpool_set_unsupported']
tags = ['functional', 'cli_root', 'zfs_sysfs']

[tests/functional/cli_root/zfs_unload-key]
tests = ['zfs_unload-key', 'zfs_unload-key_all', 'zfs_unload-key_recursive']
tags = ['functional', 'cli_root', 'zfs_unload-key']

[tests/functional/cli_root/zfs_upgrade]
tests = ['zfs_upgrade_001_pos', 'zfs_upgrade_002_pos', 'zfs_upgrade_003_pos',
    'zfs_upgrade_004_pos', 'zfs_upgrade_005_pos', 'zfs_upgrade_006_neg',
    'zfs_upgrade_007_neg']
tags = ['functional', 'cli_root', 'zfs_upgrade']

[tests/functional/cli_root/zpool]
tests = ['zpool_001_neg']
tags = ['functional', 'cli_root', 'zpool']

[tests/functional/cli_root/zpool_create]
tests = ['zpool_create_001_pos', 'create-o_ashift', 'zpool_create_tempname']
tags = ['functional', 'cli_root', 'zpool_create']

[tests/functional/cli_root/zpool_destroy]
tests = ['zpool_destroy_001_pos']
pre =
post =
tags = ['functional', 'cli_root', 'zpool_destroy']

[tests/functional/cli_root/zpool_status]
tests = ['zpool_status_001_pos', 'zpool_status_002_pos','zpool_status_003_pos',
    'zpool_status_-c_disable', 'zpool_status_-c_homedir',
    'zpool_status_-c_searchpath']
user =
tags = ['functional', 'cli_root', 'zpool_status']

[tests/functional/cp_files]
tests = ['cp_files_001_pos']
tags = ['functional', 'cp_files']

[tests/functional/ctime]
tests = ['ctime_001_pos' ]
tags = ['functional', 'ctime']

[tests/functional/rsend]
tests = ['rsend_001_pos', 'send_mixed_raw', 'send-wDR_encrypted_zvol']
tags = ['functional', 'rsend']

[tests/functional/slog]
tests = ['slog_001_pos']
tags = ['functional', 'slog']

[tests/functional/snapshot]
tests = ['clone_001_pos', 'rollback_001_pos', 'rollback_002_pos',
    'rollback_003_pos', 'snapshot_001_pos']
tags = ['functional', 'snapshot']

[tests/functional/trim]
tests = ['autotrim_integrity']
tags = ['functional', 'trim']

[tests/functional/xattr]
tests = ['xattr_001_pos', 'xattr_002_neg', 'xattr_003_neg', 'xattr_004_pos',
    'xattr_005_pos', 'xattr_006_pos', 'xattr_007_neg', 'xattr_008_pos',
    'xattr_009_neg', 'xattr_010_neg', 'xattr_011_pos', 'xattr_012_pos',
    'xattr_013_pos']
tags = ['functional', 'xattr']

[tests/functional/libzfs]
tests = ['many_fds', 'libzfs_input']
tags = ['functional', 'libzfs']
