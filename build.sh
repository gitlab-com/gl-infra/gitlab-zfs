#!/usr/bin/env bash

. ./common.sh

if [ "$UBUNTU_DIST" = "xenial" ] && echo "${KERNEL_VERSION}" | grep -qv '^4.15.'; then
  msg "Kernel ${KERNEL_VERSION} is not supported for xenial, not building."
  exit 0
fi

(
  echo "----- BUILD ENVIRONMENT -----"
  echo -n " KERNEL.: "; uname -a
  echo -n " CMDLINE: "; cat /proc/cmdline
  echo    " TARGET.: ${KERNEL_VERSION}"
  echo    " ZFS....: ${ZFS_VERSION}"
  echo    " Distro.: ${UBUNTU_DIST}"
  echo "-----------------------------"
) > build_environment_${UBUNTU_DIST}_${KERNEL_VERSION}

exec &> >(tee "build_log_${UBUNTU_DIST}_${KERNEL_VERSION}")

set -eu

msg "Installing prerequisites..."
apt update -y 2>&1 | label "package-list"
apt install -y linux-headers-${KERNEL_VERSION} 2>&1 | label "packages"
mkdir -p source_${UBUNTU_DIST}
cd source_${UBUNTU_DIST}
msg "Downloading ZFS source..."
wget -qc "https://github.com/zfsonlinux/zfs/releases/download/zfs-${ZFS_VERSION}/zfs-${ZFS_VERSION}.tar.gz" 2>&1 | label "download"
wget -qc "https://github.com/zfsonlinux/zfs/releases/download/zfs-${ZFS_VERSION}/zfs-${ZFS_VERSION}.tar.gz.asc" 2>&1 | label "download"

msg "Importing ZFS maintainer GPG keys"
gpg --import <(curl "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0xB97467AAC77B9667") 2>&1 | label "key-import" || true
gpg --import <(curl "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x6AD860EED4598027") 2>&1 | label "key-import" || true
gpg --import <(curl "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x0AB9E991C6AF658B") 2>&1 | label "key-import" || true

msg "Verifying source signature..."
gpg --verify "zfs-${ZFS_VERSION}.tar.gz.asc" 2>&1 | label "verification"

msg "Extracting source..."
tar xzf "zfs-${ZFS_VERSION}.tar.gz" 2>&1 | label "extract"

cp -rp zfs-${ZFS_VERSION} zfs-${ZFS_VERSION}-${UBUNTU_DIST}
cd "zfs-${ZFS_VERSION}-${UBUNTU_DIST}"

msg "Compiling ZFS..."
COMPILTE_TIME="$(date +%s)"
sh autogen.sh 2>&1 | label "compile"
./configure \
  --prefix=/usr \
  --sysconfdir=/etc \
  --sbindir=/usr/bin \
  --libdir=/lib \
  --datadir=/usr/share \
  --includedir=/usr/include \
  --with-udevdir=/lib/udev \
  --libexecdir=/usr/lib/zfs-${ZFS_VERSION} \
  --with-config=kernel \
  --with-linux=/lib/modules/${KERNEL_VERSION}/build \
  --with-linux-obj=/lib/modules/${KERNEL_VERSION}/build 2>&1 | label "compile"
make -s -j$(($(nproc) + 1)) 2>&1 | label "compile"
make -j1 deb 2>&1 | label "compile"

cd ../..

msg "Patching packages..."

fixpkg() {
  local tmpDir="$(mktemp -d)"
  mkdir -p "${tmpDir}"
  dpkg -I "${1}"
  dpkg-deb -c "${1}"
  dpkg-deb -R "${1}" "$tmpDir"
  rm "${1}"
  awk '$1~/Version:/ {print $1 " 1:" $2 "+gitlab'"${COMPILTE_TIME}"'"}; $1!~/Version:/ {print $0}' "$tmpDir/DEBIAN/control" | grep -v 'Depends:' | sponge "$tmpDir/DEBIAN/control"
  if [[ $1 == *"kmod-"* ]]; then
    echo -e "Depends: linux-headers-${KERNEL_VERSION}, linux-image-${KERNEL_VERSION}\n$(cat $tmpDir/DEBIAN/control)" > "${tmpDir}/DEBIAN/control"
  fi
  dpkg-deb -b "${tmpDir}" "${1}"
  rm -rf "${tmpDir}"
}

mkdir -p ./deb/${KERNEL_VERSION}/${UBUNTU_DIST}/

for deb in $(find source_${UBUNTU_DIST} -type f -name '*.deb'); do
  fixpkg "${deb}" 2>&1 | label "fixup package"
  mv ${deb} ./deb/${KERNEL_VERSION}/${UBUNTU_DIST}/
done

msg "Done"
