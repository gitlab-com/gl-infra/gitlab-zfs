esc=$(printf '\033')
label () {
  sed "s,.*,${esc}[92m$1 (${UBUNTU_DIST}):${esc}[0m &,"
}

msg () {
  echo -e "${esc}[36m$@ (${UBUNTU_DIST})${esc}[0m"
}

cleanup () {
  local rc=$?
  if [ -z "${NODE_NAME}" ]; then
    msg "NODE_NAME not set. No cleanup performed."
    exit $rc
  fi
  set +x
  msg "Cleaning up..."
  gcloud compute instances delete --quiet "${NODE_NAME}" --delete-disks=all 2>&1 | label cleanup
  exit $rc
}
