.EXPORT_ALL_VARIABLES:
KERNEL_VERSION ?= 4.15.0-1027-gcp
ZFS_VERSION ?= 0.8.0
CI_REGISTRY_IMAGE ?= registry.gitlab.com/gitlab-com/gl-infra/gitlab-zfs
IMAGE_NAME ?= $(CI_REGISTRY_IMAGE):$(UBUNTU_DIST)

DOCKER_VARS := KERNEL_VERSION ZFS_VERSION

.PHONY: all
all: docker build

.PHONY: docker
docker: Dockerfile docker-bionic docker-xenial

.PHONY: docker-bionic
docker-bionic:
	@docker build --build-arg "UBUNTU_DIST=bionic" -t $(CI_REGISTRY_IMAGE):bionic .

.PHONY: docker-xenial
docker-xenial:
	@docker build --build-arg "UBUNTU_DIST=xenial" -t $(CI_REGISTRY_IMAGE):xenial .

.PHONY: build
build: build.sh build-bionic build-xenial

.PHONY: build-bionic
build-bionic:
	docker run --rm -v "$(shell pwd)/:/build" -e UBUNTU_DIST=bionic $(foreach var,$(DOCKER_VARS), -e "$(var)=$($(var))") $(CI_REGISTRY_IMAGE):bionic /build/build.sh

.PHONY: build-xenial
build-xenial:
	docker run --rm -v "$(shell pwd)/:/build" -e UBUNTU_DIST=xenial $(foreach var,$(DOCKER_VARS), -e "$(var)=$($(var))") $(CI_REGISTRY_IMAGE):xenial /build/build.sh

.PHONY: test
test: test.sh
	./test.sh

.PHONY: clean
clean:
	rm -rf linux-* zfs-* spl-* deb/ test_* build_* source_*
