// +build linux,amd64

package main

import (
	"errors"
	"fmt"
	"golang.org/x/sys/unix"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"
)

var EModLoaded = errors.New("module already loaded")

func insmod(path string, params []string) (err error) {
	if !pathExists(path) {
		err = os.ErrNotExist
		return
	}

	var module []byte
	module, err = ioutil.ReadFile(path)
	if err != nil {
		return
	}

	err = unix.InitModule(module, strings.Join(params, " "))
	return
}

func isModuleLoaded(module string) bool {
	return pathExists(fmt.Sprintf("/sys/module/%s", modName(module)))
}

// Get 'xyz' from 'foo/xyz.ko'
func modName(module string) string {
	var basename string
	basename = path.Base(module)
	return strings.TrimSuffix(basename, filepath.Ext(basename))
}

func loadModule(release string, module string, params []string) (err error) {
	if isModuleLoaded(module) {
		err = EModLoaded
		return
	}

	if module == "zfs/zfs.ko" {
		// While zlua.ko is a dependency for the 0.8.x zfs.ko, it is not for some
		// prior versions. This is just a failsafe to make sure it IS loaded.
		err = loadModule(release, "lua/zlua.ko", nil)
		if err != nil && err != EModLoaded {
			return
		}
		err = nil
	}

	basedir := fmt.Sprintf("/lib/modules/%s/extra/zfs", release)

	if !pathExists(basedir) {
		err = os.ErrNotExist
		return
	}

	attempt(module, params)
	err = insmod(fmt.Sprintf("%s/%s", basedir, module), params)
	if err == nil {
		loaded(module, params)
	}

	return
}

func pathExists(path string) bool {
	if _, err := os.Stat(path); !os.IsNotExist(err) {
		return true
	}
	return false
}

func loaded(module string, params []string) {
	_ = ioutil.WriteFile("/dev/kmsg", []byte(fmt.Sprintf("GitLab ZFS Proxy: Loaded module '%s', params: %+v", module, params)), unix.O_APPEND)
}

func attempt(module string, params []string) {
	_ = ioutil.WriteFile("/dev/kmsg", []byte(fmt.Sprintf("GitLab ZFS Proxy: Attempt to load '%s', params: %+v", module, params)), unix.O_APPEND)
}
