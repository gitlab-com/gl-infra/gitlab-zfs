package main

import (
	"bytes"
	"errors"
	"golang.org/x/sys/unix"
)

var ENoRelease = errors.New("no release string found")

func unameR() (release string, err error) {

	buf := &unix.Utsname{}
	err = unix.Uname(buf)
	if err != nil {
		return
	}
	release = string(bytes.Trim((*buf).Release[:], "\x00"))
	if len(release) == 0 {
		err = ENoRelease
		return
	}
	return
}
