package main

import (
	"log"
	"os"
)

func quitIfErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Need to specify a module to load")
	}
	var err error
	var release string
	var module string
	var params []string

	module = os.Args[1]
	if len(os.Args) > 2 {
		params = os.Args[2:]
	}

	release, err = unameR()
	quitIfErr(err)

	err = loadModule(release, module, params)
	quitIfErr(err)
}
