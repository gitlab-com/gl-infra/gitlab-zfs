#!/usr/bin/env bash

. ./common.sh

case "$UBUNTU_DIST" in
  "bionic")
    GCP_IMAGE_NAME="ubuntu-1804-bionic-v20190514"
  ;;
  "xenial")
    GCP_IMAGE_NAME="ubuntu-1604-xenial-v20190530c"
  ;;
  "")
    msg "Need to specify \$UBUNTU_DIST"
    exit 1
  ;;
  *)
    msg "Unknown distro '${UBUNTU_DIST}'"
    exit 1
  ;;
esac

trap cleanup EXIT

set -euo pipefail

export CLOUDSDK_CORE_PROJECT="gitlab-zfs-validation"
export CLOUDSDK_COMPUTE_ZONE="us-east1-b"
NODE_NAME="$(echo pipeline-${CI_PIPELINE_ID:-manual}-job-${CI_JOB_ID:-manual} | tr '[:upper:]' '[:lower:]' | sed 's/[^a-z0-9-]/-/g')"
APT_FLAGS='-o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold"'
PACKAGES='python sudo ksh sysstat binutils attr fio'

msg "Creating VM..."
if [ -n "${GCLOUD_SERVICE_KEY:-}" ]; then
  gcloud auth activate-service-account --key-file <(echo ${GCLOUD_SERVICE_KEY}) 2>&1 | label "gcp login"
  export GCLOUD_SERVICE_KEY=""
fi

gcloud compute instances create "${NODE_NAME}" --machine-type=n1-standard-2 --subnet=default --network-tier=PREMIUM --maintenance-policy=MIGRATE --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --image=${GCP_IMAGE_NAME} --image-project=ubuntu-os-cloud --boot-disk-size=20GB --boot-disk-type=pd-ssd --boot-disk-device-name="${NODE_NAME}_disk" 2>&1 | label "create VM"

msg "Wait for machine to be booted..."
while ! gcloud compute ssh "${NODE_NAME}" -- exit; do
  sleep 5;
done 2>&1 | label boot

msg "Upgrade node & install kernel..."
gcloud compute ssh "${NODE_NAME}" <<EOF 2>&1 | label "prepare 1/2"
  set -e
  export DEBIAN_FRONTEND=noninteractive
  sudo apt update
  sudo apt ${APT_FLAGS} full-upgrade -y
  sudo add-apt-repository main
  sudo add-apt-repository universe
  sudo add-apt-repository restricted
  sudo add-apt-repository multiverse
  sudo apt update
  # Apt is racy sometimes, wait a few seconds
  sleep 5
  sudo apt ${APT_FLAGS} install -y linux-image-${KERNEL_VERSION} linux-headers-${KERNEL_VERSION} linux-modules-extra-${KERNEL_VERSION} ${PACKAGES}
  echo -e '#!/usr/bin/env bash\nexit 0' |sudo tee /usr/bin/linux-check-removal
  dpkg -l | egrep "linux-(image|modules|headers)-[0-9]+" | awk '{print \$2}' | grep -v ${KERNEL_VERSION} | xargs -r sudo apt remove -y
  sudo rm -rf /lib/modules/${KERNEL_VERSION}/kernel/zfs/
EOF

gcloud compute ssh "${NODE_NAME}" -- sudo reboot 2>&1 | label reboot || true

msg "Wait for machine to be rebooted..."
sleep 30
while ! gcloud compute ssh  "${NODE_NAME}" -- exit; do
  sleep 5;
done 2>&1 | label reboot

find . -type f -name '*devel*.deb' -delete
find . -type f -name '*dkms*.deb' -delete

msg "Copy files..."
gcloud compute scp deb/${KERNEL_VERSION}/${UBUNTU_DIST}/*.deb testsuite-gitlab.run "${NODE_NAME}:"

msg "Install packages & create test user..."
gcloud compute ssh "${NODE_NAME}" <<EOF 2>&1 | label "prepare 2/2"
  set -e
  sudo dpkg -i *.deb || true
  sudo depmod -a
  sudo modinfo zfs | grep -v parm
  ! sudo modinfo zfs | grep -q /kernel/ || exit 1
  sudo modprobe zfs
  sudo useradd -mU testuser
  echo "testuser ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/testuser
EOF

msg "Running ZFS test suite..."
gcloud compute ssh "${NODE_NAME}" <<\EOF 2>&1 | tee test_${UBUNTU_DIST}_${KERNEL_VERSION} | label "test suite"
  sudo mv testsuite-gitlab.run /usr/share/zfs/
  sudo chmod 755 /usr/share/zfs/testsuite-gitlab.run
  ls -lsa
  cd /usr/share/zfs
  time sudo -u testuser time ./zfs-tests.sh -vx -r ./testsuite-gitlab.run
EOF
